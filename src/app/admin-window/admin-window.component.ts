import { Component, OnInit, ChangeDetectionStrategy, ChangeDetectorRef, OnDestroy } from '@angular/core';
import { ServerIoService } from '../server-io.service';
import { map } from 'rxjs/operators';

@Component({
  selector: 'app-admin-window',
  templateUrl: './admin-window.component.html',
  styleUrls: ['./admin-window.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class AdminWindowComponent {

  constructor(
    public server_io: ServerIoService,
    private cd: ChangeDetectorRef,
  ) {
    console.log(this.constructor.name, this);
  }


  score_input_required = this.server_io.game_state.pipe(
    map(st => {
      return st.stage.name == "turn" && st.stage.x !== undefined
    })
  );

  data = {};
  show_reset = false;
  shot_data = { shots_left: 0 };

  game_reset() {
    this.show_reset = false;
    this.server_io.game_reset();
  }
}
