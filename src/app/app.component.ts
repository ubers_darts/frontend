import { Component, ChangeDetectionStrategy, ChangeDetectorRef } from '@angular/core';
import { MatButtonToggleChange } from '@angular/material/button-toggle';
import { ServerIoService, Player_Public } from './server-io.service';
import { map } from 'rxjs/operators';
import { slideInRightAnimation, fadeOutOnLeaveAnimation } from 'angular-animations';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  animations: [
    slideInRightAnimation(),
    fadeOutOnLeaveAnimation()
  ]
})
export class AppComponent {

  constructor(
    private cd: ChangeDetectorRef,
    public server_io: ServerIoService
  ) {
    console.log(this.constructor.name, this);
  }

  players_list = this.server_io.game_state.pipe(map(st => {
    if (!st) return [];
    const list = [];
    for (let i = 0; i < st.max_players; i++) {
      list.push((st.players[i] as Player_Public))
    }
    return list;
  }));


}



