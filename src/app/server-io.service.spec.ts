import { TestBed } from '@angular/core/testing';

import { ServerIoService } from './server-io.service';

describe('ServerIoService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ServerIoService = TestBed.get(ServerIoService);
    expect(service).toBeTruthy();
  });
});
