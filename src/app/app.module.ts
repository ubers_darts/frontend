import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatButtonModule } from '@angular/material/button';
import { MatSortModule } from '@angular/material/sort';
import { MatCardModule } from '@angular/material/card';
import { MatIconModule } from '@angular/material/icon';
import { MatSliderModule } from '@angular/material/slider';
import { SocketIoModule, SocketIoConfig } from 'ngx-socket-io';
import { FlexLayoutModule } from '@angular/flex-layout';
import { MatTooltipModule } from '@angular/material/tooltip';
import { MatSidenavModule } from '@angular/material/sidenav';
import { MatListModule } from '@angular/material/list';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { environment } from '../environments/environment';
import { ServerIoService } from './server-io.service';
import { FormsModule } from '@angular/forms';
import { PlayerSelectComponent } from './player-select/player-select.component';
import { GameTurnsComponent } from './game-turns/game-turns.component';
import { GameOverComponent } from './game-over/game-over.component';
import { AdminWindowComponent } from './admin-window/admin-window.component';


const config: SocketIoConfig = {
  url: environment.server_url,
  options: {}
};


@NgModule({
  declarations: [
    AppComponent,
    PlayerSelectComponent,
    GameTurnsComponent,
    GameOverComponent,
    AdminWindowComponent,
  ],
  imports: [
    MatProgressSpinnerModule,
    MatInputModule,
    MatFormFieldModule,
    FormsModule,
    BrowserModule,
    BrowserAnimationsModule,
    MatButtonModule,
    MatListModule,
    MatCardModule,
    FlexLayoutModule,
    MatSidenavModule,
    MatIconModule,
    MatSortModule,
    MatTooltipModule,
    MatSliderModule,
    SocketIoModule.forRoot(config),
  ],
  providers: [ServerIoService],
  bootstrap: [AppComponent]
})
export class AppModule { }
