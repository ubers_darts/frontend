import { Component, OnInit, ChangeDetectionStrategy, ChangeDetectorRef } from '@angular/core';
import { ServerIoService } from '../server-io.service';

@Component({
  selector: 'app-player-select',
  templateUrl: './player-select.component.html',
  styleUrls: ['./player-select.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class PlayerSelectComponent implements OnInit {

  constructor(
    public server_io: ServerIoService,
    private cd: ChangeDetectorRef,
  ) {
    console.log(this.constructor.name, this);
  }

  ngOnInit() {
  }

  data: { name?: string } = {};
}
