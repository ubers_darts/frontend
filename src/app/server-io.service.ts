import { Injectable } from '@angular/core';
import * as obs from 'rxjs';
import * as op from 'rxjs/operators';
import { Socket } from 'ngx-socket-io';

@Injectable({
  providedIn: 'root'
})
export class ServerIoService {

  constructor(
    private socket: Socket,
  ) {
    socket.on('game_state', (data) => {
      this.game_state.next(data);
      console.log('game_state', data);
    });

    socket.on('cookie', (data: Cookie) => {
      this.cookie = data;
      localStorage.setItem('cookie', JSON.stringify(this.cookie));
    });
    this.cookie = JSON.parse(localStorage.getItem('cookie'));
    if (this.cookie) socket.emit('test_cookie', this.cookie);


    socket.on('clear_cookie', () => {
      console.log('clear_cookie');
      delete this.cookie;
      localStorage.removeItem('cookie');
    });

    /*    socket.on('reconnect', () => {
          this.subscriptions.forEach(s => this.send_subscription(s));
        })
    
        socket.on('subscription', (data) => {
          const sub = this.subscriptions.get(data.id);
          if (sub) {
            sub.emit(data.payload);
          } else {
            console.error(`subscription id is not found: ${data.id}`);
          }
          });*/

  }

  fire(data: FireCmd) {
    data.cookie = this.cookie;
    this.socket.emit('turn', data);
  }

  join_game(data: { name: string }) {
    if (data.name=="admin") {
      if (this.admin_cookie == "admin") {
	localStorage.removeItem('admin_cookie');
	delete this.admin_cookie;
      } else {
	this.admin_cookie = "admin";
	localStorage.setItem('admin_cookie', JSON.stringify(this.admin_cookie));
      }
    } else {
      this.socket.emit('join', data);
    }
    return false;
  }

  game_state = new obs.BehaviorSubject<PublicGameState>(null);
  cookie?: Cookie;

  admin_cookie: string = JSON.parse(localStorage.getItem('admin_cookie'));

  
  send_score(score: Score_Input) {
    score.admin_cookie = this.admin_cookie;
    this.socket.emit('score_input', score);
  }

  game_reset() {
    this.socket.emit('game_reset', { admin_cookie: this.admin_cookie });
  }
}



function constObs(x: any) {
  return obs.NEVER.pipe(op.startWith(x));
};

interface Score_Input {
  score: number;
  admin_cookie?: string;
}


export interface FireCmd {
  x: number;
  y: number;
  cookie?: Cookie;
}

export interface Cookie {
  cookie: string;
  player: number;
}

export interface PublicGameState {
  players: Player_Public[];
  max_players: number;
  stage: Player_Select | Turn | Pause | Game_Over;
}

export interface Game_Over {
  name: "game_over"
}


export interface Player_Select {
  name: "player_select";
  players: number;
  max_players: number;
};

export interface Pause {
  name: "pause";
};
export interface Turn {
  name: "turn";
  player: number;
  shot: number;
  shots: number;
  round: number;
  rounds: number;
  x?: number;
  y?: number;
}


export interface Player_Public {
  name: string;
  score: number;
  score_add?: number;
}
