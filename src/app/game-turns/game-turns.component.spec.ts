import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GameTurnsComponent } from './game-turns.component';

describe('GameTurnsComponent', () => {
  let component: GameTurnsComponent;
  let fixture: ComponentFixture<GameTurnsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GameTurnsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GameTurnsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
