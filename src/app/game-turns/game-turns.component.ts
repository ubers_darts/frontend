import { Component, OnInit, ChangeDetectionStrategy, ChangeDetectorRef, OnDestroy } from '@angular/core';
import { ServerIoService, Turn } from '../server-io.service';
import { map, share, take, tap } from 'rxjs/operators';
import { interval, combineLatest } from 'rxjs';

@Component({
  selector: 'app-game-turns',
  templateUrl: './game-turns.component.html',
  styleUrls: ['./game-turns.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class GameTurnsComponent {

  constructor(
    public server_io: ServerIoService,
    private cd: ChangeDetectorRef,
  ) {
    console.log(this.constructor.name, this);
  }



  turn_state = this.server_io.game_state.pipe(
    tap(() => {
      delete this.halt_x;
      delete this.halt_y;
    }),
    map(x => x.stage as Turn),
  )

  ticks = interval(17);
  halt_x?: number;
  halt_y?: number;

  
  fire() {
    this.reticle_coords.pipe(take(1)).
      subscribe((dta) => {
	this.halt_x = dta.x;
	this.halt_y = dta.y;
	this.server_io.fire(dta)
      });
  }

  fire_button_disabled = this.turn_state.pipe(
    map(st => {
      if ((st as Turn).x !== undefined) return true;
       if (!this.server_io.cookie || this.server_io.cookie.player !== st.player) return true;
    })
  );

  awaiting_turn = this.turn_state.pipe(
    map(st => {
      if ((st as Turn).x !== undefined) return false;
      if (!this.server_io.cookie || this.server_io.cookie.player !== st.player) return true;
    })
  );


  reticle_coords = combineLatest(
    this.turn_state,
    this.ticks,
  ).pipe(
    map(d => {
      const ts = d[0];
      const t = d[1];
      if (!ts || ts.x !== undefined) {
        return { x: ts.x, y: ts.y };
      } else if (this.halt_x != undefined) {
	return {
	  x: this.halt_x,
	  y: this.halt_y,
	}
      } else if(this.server_io.cookie && this.server_io.cookie.player === ts.player) {
        return {
          x: Math.sin(t / 30) * 500 + 500,
          y: Math.sin(t / 52) * 500 + 500,
        };
      } else return {
	x: -100,
	y: -100,
      }
    }),
    share()
  );

}

