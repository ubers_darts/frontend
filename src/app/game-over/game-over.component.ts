import { Component, OnInit, ChangeDetectionStrategy, ChangeDetectorRef, OnDestroy } from '@angular/core';
import { ServerIoService, Player_Public } from '../server-io.service';
import { map, share } from 'rxjs/operators';


@Component({
  selector: 'app-game-over',
  templateUrl: './game-over.component.html',
  styleUrls: ['./game-over.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class GameOverComponent {

  constructor(
    public server_io: ServerIoService,
    private cd: ChangeDetectorRef,
  ) {
    console.log(this.constructor.name, this);
  }

  winners = this.server_io.game_state.pipe(
    map(st => {
      const players = st.players;
      let out: Player_Public[];
      let max_score = -1;
      for (const p of players) {
        if (p.score > max_score) {
          out = [p];
          max_score = p.score;
        } else if (p.score === max_score) {
          out.push(p);
        }
      }
      return out;
    }),

  );
}
